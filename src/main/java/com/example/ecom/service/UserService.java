package com.example.ecom.service;

import java.util.Optional;

import com.example.ecom.entity.User;



public interface UserService {
	  User saveUser(User user);

	    Optional<User> findByUsername(String username);

//	    void changeRole(Role newRole, String username);

		boolean signIn(User user);
}
