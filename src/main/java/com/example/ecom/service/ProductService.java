package com.example.ecom.service;

import java.util.List;

import com.example.ecom.entity.Product;

public interface ProductService {

    Product saveProduct(Product product);

	String deleteProduct(Long productId);

    List<Product> findAllProducts();
}
