package com.example.ecom.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ecom.entity.Product;
import com.example.ecom.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Product saveProduct(Product product) {
		product.setCreateTime(LocalDateTime.now());
		return productRepository.save(product);
	}
	
	   @Override
	    public String deleteProduct(Long id)
	    {
		   if(productRepository.existsById(id)) {
		        productRepository.deleteById(id);

		   }else {
			   return "Item doesn't exsists";
		   }
		   
		   if(productRepository.existsById(id)!=true){
			   return "Item Delted";
		   }
		   else {
			   return "Couldn't delete Item with ID : " + id;
		   }
	    }

	    @Override
	    public List<Product> findAllProducts()
	    {
	        return productRepository.findAll();
	    }

}
