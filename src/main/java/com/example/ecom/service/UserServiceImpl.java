package com.example.ecom.service;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ecom.entity.Role;
import com.example.ecom.entity.User;
import com.example.ecom.repository.UserRepository;


@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;


    @Override
    public User saveUser(User user)
    {
//        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(Role.USER);
        user.setCreateTime(LocalDateTime.now());

        return userRepository.save(user);
    }

    @Override
    public Optional<User> findByUsername(String username)
    {
        return userRepository.findByUsername(username);
    }

//    @Override
//    @Transactional //Transactional is required when executing an update/delete query.
//    public void changeRole(Role newRole, String username)
//    {
//        userRepository.updateUserRole(username, newRole);
//    }


	@Override
	public boolean signIn(User user) {

		boolean name = userRepository.findByUsername(user.getUsername()).isEmpty() ? false : true;
		System.out.println("Username ------------------------------------------ "+userRepository.findByUsername(user.getUsername()));
		System.out.println("Password ------------------------------------------ "+userRepository.findByPassword(user.getUsername()));
		boolean password = userRepository.findByPassword(user.getPassword()).isEmpty() ? false : true;
		
		return name && password ? true : false ;
	}


}
