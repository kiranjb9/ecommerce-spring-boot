package com.example.ecom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.ecom.entity.User;
import com.example.ecom.service.UserService;



@RestController
@RequestMapping("/api/authentication")
public class AuthenticationController {

	@Autowired
	private UserService userService;
	
    
    @PostMapping("/signup")
	public ResponseEntity<User> signUp(@RequestBody User user){
    	System.out.println("ENter end point");
		User savedUser = userService.saveUser(user);
		return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
		
	}
    
    @PostMapping("/signin")
    public boolean signIn(@RequestBody User user){
    	
    return userService.signIn(user) ? true : false;
}
    
	@GetMapping("/get")
	public String index() {
		return "Hello world";
	}
}
